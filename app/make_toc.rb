#!/usr/bin/env ruby
# frozen_string_literal: true

require 'taglib'
require 'erb'
require 'trollop'

def metadata(filename, field)
  TagLib::FileRef.open(filename.to_s) do |fileref|
    unless fileref.null?
      raise "File [#{filename}] is missing field [#{field}]" unless fileref.tag.send(field.to_sym)
      return fileref.tag.send(field.to_sym)
    end
  end
end

def flac_files(dirname)
  Dir.entries(dirname).select { |f| f.to_s.split('.')[-1] == 'flac' }.map do |f|
    full_path(dirname, f)
  end
end

def track_tuples(files)
  tracks = []
  files.each do |filename|
    raise "ERROR: File [#{filename}] does not exist." unless File.exist?(filename)
    track_title = metadata filename, 'title'
    tracks << { title: track_title, filename: filename.sub('.flac', '.wav') }
  end
  tracks
end

def create_toc(opts)
  directory = normalize_directory(opts[:directory])
  raise "ERROR: Directory [#{directory}] does not exist." unless Dir.exist? directory
  set_variables(opts, directory)
  puts ERB.new(IO.read('./templates/toc.erb')).result
end

private

def normalize_directory(directory)
  directory ||= '.'
  return directory unless directory =~ %r{/\/$/}
  directory.sub(%r{/\/$/}, '')
end

def full_path(directory, filename)
  "#{directory}/#{filename}"
end

def set_variables(opts, directory)
  files = flac_files directory.to_s
  first_track = files[0]
  @artist = opts[:artist] || metadata(first_track, 'artist')
  @album = opts[:album] || metadata(first_track, 'album')
  @tracks = track_tuples files
end

if $PROGRAM_NAME == __FILE__
  opts = Trollop.options do
    opt :artist, 'Artist', type: :string
    opt :album, 'Album', type: :string
    opt :directory, 'Directory', type: :string
  end
  create_toc opts
end
