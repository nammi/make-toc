# make-toc

[![pipeline status](https://gitlab.com/nammi/make-toc/badges/master/pipeline.svg)](https://gitlab.com/nammi/make-toc/commits/master) [![coverage report](https://gitlab.com/nammi/make-toc/badges/master/coverage.svg)](https://gitlab.com/nammi/make-toc/commits/master)

Make TOC file from FLAC files.
