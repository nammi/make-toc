require 'spec_helper.rb'
require_relative '../app/make_toc.rb'
RSpec.describe 'make_toc' do
  describe('flac_files') do
    context('with an empty directory') do
      it('returns an empty list') do
        allow(Dir).to receive(:entries).with('.').and_return([])
        expect(flac_files('.')).to be_empty  
      end
    end
    context('with a directory with no FLAC files') do
      it('returns an empty list') do
        allow(Dir).to receive(:entries).with('.').and_return(['filename.wav', 'filename.ogg', 'filename.mp3'])
        expect(flac_files('.')).to be_empty
      end
    end
    context('with a directory with one FLAC file') do
      it('returns a list with that FLAC filename') do
        allow(Dir).to receive(:entries).with('.').and_return(['filename.flac', 'filename.ogg', 'filename.mp3'])
        expect(flac_files('.').size).to eq 1
      end
    end
  end
end